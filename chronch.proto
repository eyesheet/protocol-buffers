syntax = "proto3";

import "google/protobuf/empty.proto";

package eyesheet.chronch;

service Chronch {
	// timer_total: uuid -> duration
	rpc TimerTotal(TimerTotalRequest) returns (TimerTotalResponse);
	// is_timer_paused: uuid -> bool
	rpc IsTimerPaused(IsTimerPausedRequest) returns (IsTimerPausedResponse);
	// timer_remaining: uuid -> duration
	rpc TimerRemaining(TimerRemainingRequest) returns (TimerRemainingResponse);
	// timer_end: uuid -> time?
	rpc TimerEnd(TimerEndRequest) returns (TimerEndResponse);
	// pause_timer: uuid -> void
	rpc PauseTimer(PauseTimerRequest) returns (PauseTimerResponse);
	// resume_timer: uuid -> void
	rpc ResumeTimer(ResumeTimerRequest) returns (ResumeTimerResponse);
	// get_timer_name: uuid -> string
	rpc GetTimerName(GetTimerNameRequest) returns (GetTimerNameResponse);
	// set_timer_name: uuid -> string -> void
	rpc SetTimerName(SetTimerNameRequest) returns (SetTimerNameResponse);
	// extend_timer: uuid -> duration -> void
	rpc ExtendTimer(ExtendTimerRequest) returns (ExtendTimerResponse);
	// shorten_timer: uuid -> duration -> void
	rpc ShortenTimer(ShortenTimerRequest) returns (ShortenTimerResponse);
	// delete_timer: uuid -> void
	rpc DeleteTimer(DeleteTimerRequest) returns (DeleteTimerResponse);
	// reset: uuid -> void
	rpc Reset(ResetRequest) returns (ResetResponse);
	// forward: uuid -> void
	rpc Forward(ForwardRequest) returns (ForwardResponse);
	// rewind: uuid -> void
	rpc Rewind(RewindRequest) returns (RewindResponse);
	// set_total: uuid -> duration -> void
	rpc SetTotal(SetTotalRequest) returns (SetTotalResponse);

	// new_timer_ending_lasting_duration_at_time: duration -> time -> uuid
	rpc NewTimerEndingLastingDurationAtTime(NewTimerEndingLastingDurationAtTimeRequest) returns (NewTimerEndingLastingDurationAtTimeResponse);
	// new_timer_lasting:  duration -> uuid
	rpc NewTimerLasting(NewTimerLastingRequest) returns (NewTimerLastingResponse);
	// new_timer_lasting_with_remaining:  duration -> duration -> uuid
	rpc NewTimerLastingWithRemaining(NewTimerLastingWithRemainingRequest) returns (NewTimerLastingWithRemainingResponse);

	rpc TimerStatuses(google.protobuf.Empty) returns (stream TimerStatus);
	rpc TimerStatusesOnce(google.protobuf.Empty) returns (RepeatedTimerStatus);
}

message TimerTotalRequest {
	string uuid = 1;
}

message IsTimerPausedRequest {
	string uuid = 1;
}

message TimerRemainingRequest {
	string uuid = 1;
}

message TimerEndRequest {
	string uuid = 1;
}

message PauseTimerRequest {
	string uuid = 1;
}

message ResumeTimerRequest {
	string uuid = 1;
}

message GetTimerNameRequest {
	string uuid = 1;
}

message SetTimerNameRequest {
	string uuid = 1;
	string new_name = 2;
}

message ExtendTimerRequest {
	string uuid = 1;
	int64 duration = 2;
}

message ShortenTimerRequest {
	string uuid = 1;
	int64 duration = 2;
}

message DeleteTimerRequest {
	string uuid = 1;
}

message ResetRequest {
	string uuid = 1;
}

message ForwardRequest {
	string uuid = 1;
	int64 amount = 2;
}

message RewindRequest {
	string uuid = 1;
	int64 amount = 2;
}

message SetTotalRequest {
	string uuid = 1;
	int64 duration = 2;
}

message NewTimerEndingLastingDurationAtTimeRequest {
	int64 end = 1;
	uint64 total = 2;
}

message NewTimerLastingRequest {
	int64 total = 1;
}

message NewTimerLastingWithRemainingRequest {
	int64 total = 1;
	int64 remaining = 2;
}

message TimerTotalResponse {
	int64 total = 1;
}

message IsTimerPausedResponse {
	bool isPaused = 1;
}

message TimerRemainingResponse {
	int64 remaining = 1;
}

message TimerEndResponse {
	optional uint64 end = 1;
}

message PauseTimerResponse {}

message ResumeTimerResponse {}

message GetTimerNameResponse {
	string name = 1;
}

message SetTimerNameResponse {}

message ExtendTimerResponse {}

message ShortenTimerResponse {}

message DeleteTimerResponse {}

message ResetResponse {}

message ForwardResponse {}

message RewindResponse {}

message SetTotalResponse {}

message NewTimerEndingLastingDurationAtTimeResponse {
	string uuid = 1;
}

message NewTimerLastingResponse {
	string uuid = 1;
}

message NewTimerLastingWithRemainingResponse {
	string uuid = 1;
}

message UuidRequest {
	string uuid = 1;
}

message TimerStatus {
	string uuid = 1;
	/// Describes when the timer completes
	string name = 2;
	int64 total = 3;
	oneof tail {
		int64 remaining = 4;
		int64 end = 5;
	}
}

message RepeatedTimerStatus {
	repeated TimerStatus statuses = 1;
}
